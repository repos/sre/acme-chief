module gitlab.wikimedia.org/repos/sre/acme-chief

go 1.19

require github.com/letsencrypt/pebble v1.0.1

require (
	github.com/jmhodges/clock v0.0.0-20160418191101-880ee4c33548 // indirect
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9 // indirect
	gopkg.in/square/go-jose.v2 v2.1.9 // indirect
)
