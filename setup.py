import os
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

install_requires = [
    'acme >= 2.1.0',
    'cryptography >= 38.0.4',
    'dnspython >= 2.3.0',
    'flask >= 2.2.2',
    'josepy >= 1.13.0',
    'pyOpenSSL >= 23.0.0',
    'requests >= 2.28.1',
    'pyyaml >= 6.0',
    'sdnotify >= 0.3.1',
    # Werkzeug 3.x removed some functions required by our version of flask
    # https://github.com/pallets/werkzeug/pull/2768
    'werkzeug == 2.3.7',
]

extras_require = {
    # Test dependencies
    'tests': [
        'pylint',
        'pytest-cov >= 4.0.0',
        'dnslib >= 0.9.23',
        'requests-mock >= 1.9.3',
    ]
}

# Generate minimum dependencies
extras_require['tests-min'] = [dep.replace('>=', '==') for dep in extras_require['tests']]
if os.getenv('ACMECHIEF_MIN_DEPS', False):
    install_requires = [dep.replace('>=', '==') for dep in install_requires]

setuptools.setup(
    name="acme-chief",
    version="0.38",
    author="Alex Monk",
    author_email="krenair@gmail.com",
    description="Python application to request certificates from ACME servers and distribute to authorised clients.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://phabricator.wikimedia.org/diffusion/OSCC/",
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'acme-chief-backend = acme_chief.acme_chief:main'
        ]
    },
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ),
    install_requires=install_requires,
    extras_require=extras_require
)
